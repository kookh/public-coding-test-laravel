<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    public $timestamps = false;

    /** @var array */
    protected $fillable = [
        'user_id',
        'number',   //주문 번호
        'completed_at', //주문 완료 날짜
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
        'number' => 'string',
        'completed_at' => 'datetime',
    ];

    /**
     * 주문 - 상품 
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product', 'order_id', 'product_id');
    }
}
