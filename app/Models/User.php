<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'nick_name',
        'phone',
        'email',
        'sex'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'password' => 'string',
        'nick_name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'sex' => 'boolean'
    ];

    /**
     * 회원 - 주문 
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id')
            ->with('products:name')->orderBy('completed_at', 'desc');
    }

    /**
     * 회원 - 마지막 주문
     */
    public function lastOrder()
    {
        return $this->hasOne(Order::class, 'user_id')
            ->with('products:name')->orderBy('completed_at', 'desc');
    }
}
