<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    /**
     * 로그인
     *
     * @param Request $request
     * @return void
     */
    public function signin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:100',
            'password' => 'required|min:10|max:100|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{10,}$/'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => '입력 값 유효성이 실패하였습니다.',
                'error' => $validator->errors()
            ], 400);
        }

        //email과 password가 회원테이블에 있는지 확인
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => '이메일 또는 패스워드가 일치 하지 않습니다.',
                'error' => 'incorrectInput'
            ], 400);
        }
        $user = User::where('email', $request['email'])->first();
        //토큰 생성
        $token = $user->createToken('Personal Acess Token')->accessToken;

        return response()->json([
            'message' => '로그인 성공',
            'token' => $token
        ], 200);
    }

    /**
     * 회원가입
     * 
     * @param Request $request
     */
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:20|regex:/^[ㄱ-ㅎ가-힣0-9a-zA-Z]*$/',
            'nick_name' => 'required|unique:users|max:30|regex:/^[a-z]*$/',
            'email' => 'required|email|unique:users|max:100',
            'phone' => 'required|regex:/^[0-9]*$/|min:11|max:11',
            'password' => 'required|confirmed|min:10|max:100|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{10,}$/',
            'sex' => 'boolean|nullable'
        ]);
        //패스워드 정규식 참조
        //http://frontend.diffthink.kr/2018/02/blog-post.html 
        //https://www.it-swarm.dev/ko/javascript/%EC%95%94%ED%98%B8%EC%97%90-%EB%8C%80%ED%95%9C-%EC%A0%95%EA%B7%9C%EC%8B%9D%EC%9D%80-8-%EC%9E%90-%EC%9D%B4%EC%83%81-%ED%95%98%EB%82%98-%EC%9D%B4%EC%83%81%EC%9D%98-%EC%88%AB%EC%9E%90-%EB%B0%8F-%EB%8C%80%EB%AC%B8%EC%9E%90%EC%99%80-%ED%8A%B9%EC%88%98-%EB%AC%B8%EC%9E%90-%EB%AA%A8%EB%91%90%EB%A5%BC-%ED%8F%AC%ED%95%A8%ED%95%B4%EC%95%BC%ED%95%A9%EB%8B%88%EB%8B%A4/1043325838/


        if ($validator->fails()) {
            return response()->json([
                'message' => '입력 값 유효성이 실패하였습니다.',
                'error' => $validator->errors()
            ], 400);
        }

        $user = User::create([
            'name' => $request['name'],
            'nick_name' => $request['nick_name'],
            'password' => bcrypt($request['password']),
            'phone' => $request['phone'],
            'email' => $request['email'],
            'sex' => $request['sex']
        ]);

        //토큰 생성
        $token = $user->createToken('Personal Acess Token')->accessToken;

        return response()->json([
            'message' => '회원가입 성공',
            'token' => $token
        ], 201);
    }

    /**
     * 로그아웃
     */
    public function signout()
    {
        $user = request()->user();
        $user->token()->revoke();

        return response()->json([
            'message' => '로그아웃 성공'
        ], 200);
    }
}
