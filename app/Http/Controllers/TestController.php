<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use Faker\Generator as Faker;
use Validator;
use DB;

class TestController extends Controller
{
    /**
     * 주문 하기
     */
    public function addOrder(Request $request, Faker $faker)
    {
        $user = request()->user();
        $productCount = Product::whereIn('id', $request->products)->count();

        if ($productCount !== count($request->products)) {
            return response()->json([
                'message' => '주문 목록에 선택 할 수 없는 제품이 포함되어 있습니다.'
            ], 400);
        }

        DB::transaction(function () use ($request, $user, $faker) {
            $order = Order::create([
                'user_id' => $user->id,
                'number' =>  'A' . (string) $faker->unixTime($max = 'now'),
                'completed_at' => date("Y-m-d H:i:s"),
            ]);

            $order->products()->sync($request->products);
        });

        return response()->json([
            'message' => '주문하기 성공',
            'data' => $request->products
        ], 201);
    }

    /**
     * 제품 추가
     */
    public function addProduct(Request $request)
    {
        request()->user();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => '입력 값 유효성이 실패하였습니다.',
                'error' => $validator->errors()
            ], 400);
        }

        $product = Product::create([
            'name' => $request['name'],
        ]);

        return response()->json([
            'message' => '제품 추가 성공',
            'data' => $product
        ], 201);
    }

    /**
     * 모든 제품 가져오기
     */
    public function getProducts()
    {
        request()->user();

        $products = Product::get();

        return response()->json([
            'message' => '모든 제품 가져오기 성공',
            'data' => $products
        ], 201);
    }
}
