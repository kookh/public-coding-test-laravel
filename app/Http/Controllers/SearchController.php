<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * 단일회원 정보보기
     *
     * @param integer $id  user id
     */
    public function getUser(int $id)
    {
        request()->user();
        $user = User::findOrFail($id);

        return response()->json([
            'message' => '단일회원 정보보기 성공',
            'data' => $user
        ], 200);
    }


    /**
     * 여러 회원 목록 조회 (페이지네이션)
     */
    public function getAllUsers()
    {
        request()->user();
        $users = User::orderBy('created_at', 'desc')->paginate(3);

        return response()->json([
            'message' => '여러 회원 목록 조회 성공',
            'data' => $users
        ], 200);
    }

    /**
     * 여러 회원 목록 조회 (이름, 이메일을 이용하여 검색 기능)
     */
    public function getSearchUser(Request $request)
    {
        request()->user();
        $name = $request->input('name');
        $email = $request->input('email');

        $user = null;
        if ($name && $email) {
            $user = User::where('name', $name)->where('email', $email)->firstOrFail();
        } elseif ($name) {
            $user = User::where('name', $name)->get();
        } elseif ($email) {
            $user = User::where('email', $email)->firstOrFail();
        }

        return response()->json([
            'message' => '이름, 이메일을 이용하여 검색 성공',
            'data' => $user
        ], 200);
    }

    /**
     * 여러 회원 목록 조회 (각 회원의 마지막 주문 정보)
     */
    public function getLastOrder()
    {
        request()->user();
        $users = User::with('lastOrder')->get();

        return response()->json([
            'message' => '각 회원의 마지막 주문 정보 조회 성공',
            'data' => $users
        ], 200);
    }


    /**
     * 단일 회원의 주문 목록 조회 
     * 
     * @param integer $id  user id
     */
    public function getOrders(int $id)
    {
        request()->user();
        $user = User::findOrFail($id);

        return response()->json([
            'message' => '단일 회원의 주문 목록 조회 성공',
            'data' => $user->orders()->get()
        ], 200);
    }
}
