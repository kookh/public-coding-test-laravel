### DB 설계
![db 설계](./image/db1.png)
##### 코드 위치 : app/Models
##### 테이블 생성 쿼리문 위치: database/migrations

### API 설계
![api 설계](./image/api1.png)

##### 코드 위치 : app/Http/Controllers
아래 링크에서 Name의 Open을 클릭하시면 각 API의 Requesst/Response 내용을 볼 수 있습니다.

https://www.notion.so/8c272b01034d40f0ba0fe90cbf52cfdb?v=66d84aa0267c410481b7a90778e8925b

### 개발환경 구성
- Docker Image로 개발환경 구성
1) Laravel 5.8
2) PHP 7.1
3) MariaDB 5.5.56

* 현재 노트북 성능문제로 php 7.3 이상을 사용하지 못했습니다.