<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    /**
     * [인증 API]
     */
    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::post('/signup', 'AuthController@signup');   //회원가입      
        Route::post('/signin', 'AuthController@signin');   //로그인  
        Route::middleware('auth:api')->get('/signout', 'AuthController@signout'); //로그아웃
    });

    /**
     * [검색 API]
     */
    Route::group([
        'prefix' => 'search',
        'middleware' => 'auth:api',
    ], function () {
        Route::get('/user/{id}', 'SearchController@getUser');      //단일 회원 상세 정보 조회   
        Route::get('/user/{id}/orders', 'SearchController@getOrders');   //단일 회원의 주문 목록 조회 
        Route::get('/users', 'SearchController@getAllUsers');            //여러 회원 목록 조회 (페이지네이션)
        Route::get('/users/search', 'SearchController@getSearchUser');   //여러 회원 목록 조회 (이름, 이메일을 이용하여 검색)
        Route::get('/users/last-order', 'SearchController@getLastOrder');  //여러 회원 목록 조회 (각 회원의 마지막 주문 정보)
    });

    /**
     * [테스트 API]
     */
    Route::group([
        'prefix' => 'test',
        'middleware' => 'auth:api',
    ], function () {
        Route::post('/product', 'TestController@addProduct');    //제품 추가    
        Route::get('/products', 'TestController@getProducts');    //제품 목록 
        Route::post('/order', 'TestController@addOrder');        //주문 하기      
    });
});
